"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useLoUrl = useLoUrl;

function useLoUrl(props) {
  // let routeStateData = props.history.location.state;
  if (!(props && props.resource)) {
    return {};
  }

  var item = props.resource;
  var type = item.subType;
  var url = item[type].url;
  var questionCount = item[type].cupOptions.questionCount;
  var totalScore = item[type].cupOptions.totalScore;
  var passScore = item.passScore;
  return {
    url: props.baseUrl + "/" + url,
    resourceId: item.resource,
    questionCount: questionCount,
    totalScore: totalScore,
    passScore: passScore,
    itemCode: item.itemCode
  };
}