"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _testRunner = _interopRequireDefault(require("../LORunner/test-runner"));

var _reactRouter = require("react-router");

var _TestStartPage = _interopRequireDefault(require("../LORunner/TestStartPage"));

var _TestSummaryPage = _interopRequireDefault(require("../LORunner/TestSummaryPage"));

var _LoUrlHook = require("./LoUrlHook");

var _testService = require("../Services/testService");

var _loItem = require("./loItem.hook");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function Test(props) {
  var status = (0, _LoUrlHook.useLoUrl)(props);
  var currentState = (0, _loItem.useItemState)(status.itemCode);

  function startTest() {
    if (currentState && currentState.fetchedState && currentState.fetchedState.status !== 'InProgress') {
      currentState.setFetchedState({});
      (0, _testService.setItemState)(status.itemCode, {}).then(function (succ) {
        props.history.push({
          pathname: props.location.pathname + "/" + "on",
          state: {
            url: status.url,
            resourceId: status.resourceId,
            totalScore: status.totalScore,
            passScore: status.passScore,
            itemCode: status.itemCode
          }
        });
      }).catch(function (err) {
        return console.log(err);
      });
    } else {
      props.history.push({
        pathname: props.location.pathname + "/" + "on",
        state: {
          url: status.url,
          resourceId: status.resourceId,
          totalScore: status.totalScore,
          passScore: status.passScore,
          itemCode: status.itemCode
        }
      });
    }
  }

  function showResult(score, review) {
    props.history.push({
      pathname: props.location.pathname + "/" + "summary",
      state: {
        score: score,
        review: review,
        resourceId: status.resourceId,
        totalScore: status.totalScore,
        passScore: status.passScore,
        itemCode: status.itemCode
      }
    });
  }

  var sendPageStatus = function sendPageStatus(page) {
    if (props.on) {
      props.on(page);
    }
  };

  return _react.default.createElement(_react.default.Fragment, null, props.baseUrl && props.resource && _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_reactRouter.Route, {
    exact: true,
    path: "".concat(props.match.path, "/on/summary"),
    render: function render() {
      sendPageStatus('summary');
      return _react.default.createElement(_TestSummaryPage.default, _extends({
        currentState: currentState,
        next: props.next
      }, props));
    }
  }), _react.default.createElement(_reactRouter.Route, {
    exact: true,
    path: "".concat(props.match.path, "/on"),
    render: function render() {
      sendPageStatus('progress');
      return _react.default.createElement(_testRunner.default, _extends({
        currentState: currentState,
        showResult: showResult
      }, props));
    }
  }), _react.default.createElement(_reactRouter.Route, {
    exact: true,
    path: "".concat(props.match.path),
    render: function render() {
      sendPageStatus('start');
      return _react.default.createElement(_TestStartPage.default, _extends({
        fetchedState: currentState.fetchedState,
        startTest: startTest,
        status: status
      }, props));
    }
  })));
}

var _default = Test;
exports.default = _default;