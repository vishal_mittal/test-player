"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.scope = void 0;

var _reactIntl = require("react-intl");

/*
 * PracticePage Messages
 *
 * This contains all the text for the PracticePage component.
 */
var scope = 'boilerplate.containers.Test';
exports.scope = scope;

var _default = (0, _reactIntl.defineMessages)({
  heading: {
    id: "".concat(scope, ".teststart.listdata.heading"),
    defaultMessage: 'Start Page List Data'
  },
  instruction1: {
    id: "".concat(scope, ".teststart.listdata.message1"),
    defaultMessage: 'Start Page List Data'
  },
  instruction2: {
    id: "".concat(scope, ".teststart.listdata.message2"),
    defaultMessage: 'Start Page List Data'
  },
  instruction3: {
    id: "".concat(scope, ".teststart.listdata.message3"),
    defaultMessage: 'Start Page List Data'
  },
  startButton: {
    id: "".concat(scope, ".teststart.startbutton.message"),
    defaultMessage: 'START QUIZ'
  },
  resumeButton: {
    id: "".concat(scope, ".teststart.resumebutton.message"),
    defaultMessage: 'RESUME QUIZ NOW'
  },
  question: {
    id: "".concat(scope, ".teston.question.message"),
    defaultMessage: 'Question'
  },
  checkWorkButton: {
    id: "".concat(scope, ".teston.checkworkbutton.message"),
    defaultMessage: 'CHECK MY WORK'
  }
});

exports.default = _default;