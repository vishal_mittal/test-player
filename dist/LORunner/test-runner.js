"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _styles = require("@material-ui/styles");

var _loRunner = _interopRequireDefault(require("./lo-runner"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _reactIntl = require("react-intl");

var _Test = _interopRequireDefault(require("./Test.messages"));

var _ErrorBoundary = _interopRequireDefault(require("containers/ErrorBoundary"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\nbackground-color:#f2f2f2;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\ncolor:", ";\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\ncolor:white;\n&:disabled{\n    color:#616161;\n}\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\n&:hover{\n    background-color:", ";\n}\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\npadding:", ";\nbackground-color:", ";\ncolor:white;\nbox-shadow:", ";\nfont-weight:500;\nwidth:50%;\n&:disabled{\n    // background-color:rgba(255,255,255,0.5);\n    color:white;\n    opacity:0.5;\n    box-shadow:none;\n}\n// &:hover:disabled{\n//     background-color:rgba(255,255,255,0.5);\n// }\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var styles = function styles(theme) {
  return {
    upToSmall: _defineProperty({}, theme.breakpoints.down('sm'), {
      display: 'none'
    }),
    aboveSmall: _defineProperty({}, theme.breakpoints.up('md'), {
      display: 'none'
    }),
    paddingAdjustment: _defineProperty({}, theme.breakpoints.up('md'), {
      paddingBottom: theme.spacing(2)
    })
  };
};

function QuestionNumber(props) {
  return _react.default.createElement(_core.Box, {
    fontWeight: 600,
    flexGrow: 1,
    pl: 3,
    pr: 0.5,
    pt: 2,
    pb: 1
  }, _react.default.createElement(_core.Typography, {
    color: "inherit",
    variant: "h6"
  }, _react.default.createElement(_reactIntl.FormattedMessage, _Test.default.question), "\xA0", props.current, "/", props.total));
}

var CheckWorkButton = (0, _styledComponents.default)(_core.Button)(_templateObject(), function (props) {
  return props.theme.spacing(0.5, 1);
}, function (props) {
  return props.theme.palette.secondary.main;
}, function (props) {
  return props.theme.shadows[4];
});
var NextButton = (0, _styledComponents.default)(CheckWorkButton)(_templateObject2(), function (props) {
  return props.theme.palette.secondary.main;
});
var StyledButton = (0, _styledComponents.default)(_core.Button)(_templateObject3());
var NavigationBox = (0, _styledComponents.default)(_core.Box)(_templateObject4(), function (props) {
  return props.theme.tertiary.main;
});
var TestFooter = (0, _styledComponents.default)(_core.Box)(_templateObject5());

function TestRunner(props) {
  var classes = props.classes;

  var loRunnerRef = _react.default.useRef();

  var resourceId = props.location.state.resourceId;
  var totalScore = props.location.state.totalScore;
  var passScore = props.location.state.passScore;
  var itemCode = props.location.state.itemCode;
  var currState = {};
  var isReview = false;

  if (props.currentState && props.currentState.fetchedState && props.currentState.fetchedState.status === 'Completed') {
    isReview = true;
  }

  var _useState = (0, _react.useState)({
    currentQNo: 0,
    totalQNo: 0
  }),
      _useState2 = _slicedToArray(_useState, 2),
      quesNo = _useState2[0],
      setQuesNo = _useState2[1];

  var url = props.location.state.url;

  var _useState3 = (0, _react.useState)({
    isNext: true,
    isPrev: false,
    isCheckAnswer: false,
    isHint: false,
    isTryAgain: false
  }),
      _useState4 = _slicedToArray(_useState3, 2),
      state = _useState4[0],
      setState = _useState4[1];

  var _useState5 = (0, _react.useState)(false),
      _useState6 = _slicedToArray(_useState5, 2),
      testReady = _useState6[0],
      setTestReady = _useState6[1];

  function updateBtnState(newState) {
    if (newState.control === "goNext") {
      state.isNext = newState.visible;
    } else if (newState.control === "goPrev") {
      state.isPrev = newState.visible;
    } else if (newState.control === 'checkAnswers') {
      state.isCheckAnswer = newState.visible;
    } else if (newState.hint === 'hint') {
      state.isHint = newState.visible;
    } else if (newState.control === 'tryAgain') {
      state.isTryAgain = newState.visible;
    }

    setState(_objectSpread({}, state));
  }

  function updateQuestionNumbers(currQues, totQues) {
    if (currQues != undefined) {
      quesNo.currentQNo = currQues;
    } else if (totQues != undefined) {
      quesNo.totalQNo = totQues;
    }

    setQuesNo(_objectSpread({}, quesNo));
  }

  function onBtnClick(btn) {
    if (btn === 'next') {
      loRunnerRef.current('goNext');
    } else if (btn === 'previous') {
      loRunnerRef.current('goPrev');
    } else if (btn === 'checkAnswers') {
      loRunnerRef.current('checkAnswers');
    } else if (btn === 'Hint') {} else if (btn === 'sendScores') {
      // sessionStorage.removeItem(resourceId);
      loRunnerRef.current('sendScores');
    } else if (btn === 'tryAgain') {
      loRunnerRef.current('tryAgain');
    }
  }

  function onTestReady() {
    setTestReady(true);
  }

  function closePlayer() {
    // let newState = props.currentState.fetchedState
    // newState.status='InProgress'
    // isReview?null:props.currentState.setFetchedState(newState)
    props.history.goBack();
  }

  function goToSummary() {
    props.history.push({
      pathname: props.location.pathname + "/" + "summary",
      state: {
        review: true,
        resourceId: resourceId,
        totalScore: totalScore,
        passScore: passScore
      }
    });
  }

  return _react.default.createElement(_core.Box, {
    height: 1,
    overflow: "auto",
    flexDirection: "column",
    display: "flex"
  }, testReady && _react.default.createElement(_core.Box, {
    textAlign: "left",
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    bgcolor: "primary.main",
    color: "primary.contrastText"
  }, _react.default.createElement(QuestionNumber, {
    current: quesNo.currentQNo,
    total: quesNo.totalQNo
  }), _react.default.createElement(_core.IconButton, {
    onClick: closePlayer,
    color: "inherit"
  }, _react.default.createElement(_Close.default, null))), _react.default.createElement(_core.Box, {
    flexGrow: 1
  }, _react.default.createElement(_ErrorBoundary.default, null, _react.default.createElement(_loRunner.default, _extends({
    ref: loRunnerRef,
    ready: onTestReady,
    itemCode: itemCode,
    resourceId: resourceId,
    totalScore: totalScore,
    passScore: passScore,
    currState: currState,
    updateQuestionNumbers: updateQuestionNumbers,
    updateBtnState: updateBtnState,
    url: url
  }, props)))), testReady && _react.default.createElement(TestFooter, {
    p: 2,
    pb: 0,
    textAlign: "center",
    bgcolor: "primary.main"
  }, !state.isTryAgain && !isReview ? _react.default.createElement(CheckWorkButton, {
    disabled: !state.isCheckAnswer,
    onClick: function onClick() {
      onBtnClick('checkAnswers');
    }
  }, _react.default.createElement(_core.Typography, {
    variant: "body1"
  }, _react.default.createElement(_reactIntl.FormattedMessage, _Test.default.checkWorkButton))) : _react.default.createElement(NextButton, {
    onClick: function onClick() {
      state.isNext ? onBtnClick('next') : isReview ? goToSummary() : onBtnClick('sendScores');
    }
  }, _react.default.createElement(_core.Typography, {
    variant: "body1"
  }, state.isNext ? 'Next Question' : isReview ? 'Close Review' : 'Finish')), _react.default.createElement(NavigationBox, {
    pt: 1
  }, _react.default.createElement(_core.Button, {
    component: "span",
    variant: "text",
    color: "inherit",
    disabled: !state.isPrev,
    onClick: function onClick() {
      return onBtnClick('previous');
    }
  }, "Previous"), _react.default.createElement(_core.Typography, {
    component: "span",
    color: "textSecondary"
  }, "|"), _react.default.createElement(_core.Button, {
    component: "span",
    variant: "text",
    color: "inherit",
    disabled: isReview,
    onClick: function onClick() {
      state.isTryAgain ? onBtnClick('tryAgain') : state.isNext ? onBtnClick('next') : onBtnClick('sendScores');
    }
  }, state.isTryAgain || isReview ? 'Try Again' : !state.isNext ? 'Finish' : 'Next'))));
}

var _default = (0, _styles.withStyles)(styles)(TestRunner);

exports.default = _default;