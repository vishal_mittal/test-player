"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _FullWidthButton = _interopRequireDefault(require("components/Button/FullWidthButton"));

var _Close = _interopRequireDefault(require("@material-ui/icons/Close"));

var _styles = require("@material-ui/styles");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _testService = require("../Services/testService");

var _Trophy = _interopRequireDefault(require("../assets/Trophy.svg"));

var _TryAgain = _interopRequireDefault(require("../assets/TryAgain.svg"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject10() {
  var data = _taggedTemplateLiteral(["\n    background-size: 100% 100%;\n    background-image: url(", ");\n"]);

  _templateObject10 = function _templateObject10() {
    return data;
  };

  return data;
}

function _templateObject9() {
  var data = _taggedTemplateLiteral(["\n    padding:0px;\n"]);

  _templateObject9 = function _templateObject9() {
    return data;
  };

  return data;
}

function _templateObject8() {
  var data = _taggedTemplateLiteral(["\n    padding:12px 0px;\n"]);

  _templateObject8 = function _templateObject8() {
    return data;
  };

  return data;
}

function _templateObject7() {
  var data = _taggedTemplateLiteral(["\n    display:flex;\n    flex-direction:column;\n    position:relative;\n    margin-top:50px;\n    flex-grow:1;\n    border-top: 5px solid #5674b9;\n    padding:0px 24px;\n"]);

  _templateObject7 = function _templateObject7() {
    return data;
  };

  return data;
}

function _templateObject6() {
  var data = _taggedTemplateLiteral(["\n    background-size: 100% 100%;\n    background-image: url(", ");\n"]);

  _templateObject6 = function _templateObject6() {
    return data;
  };

  return data;
}

function _templateObject5() {
  var data = _taggedTemplateLiteral(["\nfont-weight : 500;\n"]);

  _templateObject5 = function _templateObject5() {
    return data;
  };

  return data;
}

function _templateObject4() {
  var data = _taggedTemplateLiteral(["\ncolor:#87990f;\n"]);

  _templateObject4 = function _templateObject4() {
    return data;
  };

  return data;
}

function _templateObject3() {
  var data = _taggedTemplateLiteral(["\nbottom:0\n"]);

  _templateObject3 = function _templateObject3() {
    return data;
  };

  return data;
}

function _templateObject2() {
  var data = _taggedTemplateLiteral(["\nbox-shadow:", ";\nz-index:1;\n"]);

  _templateObject2 = function _templateObject2() {
    return data;
  };

  return data;
}

function _templateObject() {
  var data = _taggedTemplateLiteral(["\nbackground-color:#86dafa;\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var useStyles = (0, _styles.makeStyles)(function (theme) {
  return (0, _core.createStyles)({
    borderedCard: {
      marginTop: '45px',
      flexGrow: 1
    },
    roundedDiv: {
      'background-color': "".concat(theme.palette.primary.main),
      'height': '100px',
      'width': '100px',
      'border-radius': '50%',
      'align-items': 'center',
      'display': 'flex',
      'justify-content': 'center',
      'position': 'absolute',
      'top': '0',
      'left': 'calc(50% - 50px)',
      'color': "".concat(theme.palette.primary.contrastText)
    },
    successText: {
      color: "".concat(theme.palette.error.main)
    }
  });
});
var StyledBox = (0, _styledComponents.default)(_core.Box)(_templateObject());
var ScoreBox = (0, _styledComponents.default)(_core.Box)(_templateObject2(), function (props) {
  return props.theme.shadows[5];
});
var ButtonBox = (0, _styledComponents.default)(_core.Box)(_templateObject3());
var SuccessText = (0, _styledComponents.default)(_core.Typography)(_templateObject4());
var BoldText = (0, _styledComponents.default)(_core.Typography)(_templateObject5());
var ImageDiv = (0, _styledComponents.default)(_core.Box)(_templateObject6(), _Trophy.default);
var StyledCard = (0, _styledComponents.default)(_core.Card)(_templateObject7());
var StyledCardActions = (0, _styledComponents.default)(_core.CardActions)(_templateObject8());
var StyledCardContent = (0, _styledComponents.default)(_core.CardContent)(_templateObject9());
var TryAgainImageDiv = (0, _styledComponents.default)(_core.Box)(_templateObject10(), _TryAgain.default);

function TestSummaryPage(props) {
  var currentScore = props.location.state ? props.location.state.score : 0;
  var review = props.location.state ? props.location.state.review : false;
  var totalScore = props.location.state ? props.location.state.totalScore : 7;
  var passScore = props.location.state ? props.location.state.passScore : 0.33;
  var itemCode = props.location.state ? props.location.state.itemCode : undefined;
  var result = currentScore > passScore * totalScore;

  function nextTest(props) {
    if (props.next) {
      props.next();
    }
  }

  function closePlayer() {}

  function reTake() {
    props.currentState.setFetchedState({});
    (0, _testService.setItemState)(itemCode, {}).then(function (succ) {
      props.history.goBack();
    }).catch(function (e) {
      return console.log(e);
    });
  }

  var classes = useStyles();

  function launchReviewMode() {
    props.history.goBack();
  }

  function ReviewQuiz() {
    return _react.default.createElement(_react.default.Fragment, null, "\xA0|\xA0", _react.default.createElement(_core.Link, {
      onClick: launchReviewMode
    }, "Review Quiz"));
  }

  return _react.default.createElement(StyledBox, {
    height: "inherit",
    display: "flex",
    flexDirection: "column"
  }, _react.default.createElement(_core.Box, {
    display: "flex",
    flexDirection: "row-reverse"
  }, _react.default.createElement(_core.IconButton, {
    "aria-label": "close",
    onClick: closePlayer
  }, _react.default.createElement(_Close.default, null))), _react.default.createElement(_core.Box, {
    fontWeight: "fontWeightBold",
    textAlign: "center",
    pb: 1
  }, _react.default.createElement(BoldText, {
    variant: "h5"
  }, "Your Score")), _react.default.createElement(_core.Box, {
    px: 4,
    pb: 2.5,
    minHeight: "50%",
    height: "60%",
    display: "flex",
    flexDirection: "column",
    position: "relative"
  }, _react.default.createElement(ScoreBox, {
    className: classes.roundedDiv,
    fontWeight: "fontWeightBold"
  }, _react.default.createElement(_core.Typography, {
    variant: "h5"
  }, currentScore, "/", totalScore)), _react.default.createElement(StyledCard, null, _react.default.createElement(StyledCardContent, {
    className: classes.borderedCard
  }, _react.default.createElement(_core.Box, {
    height: "60%",
    width: "100%"
  }, result ? _react.default.createElement(ImageDiv, {
    position: "relative",
    height: "80%",
    width: "100%"
  }) : _react.default.createElement(TryAgainImageDiv, {
    position: "relative",
    height: "100%",
    width: "100%"
  })), _react.default.createElement(_core.Box, {
    textAlign: "center",
    height: "40%"
  }, result ? _react.default.createElement(SuccessText, {
    variant: "h6"
  }, "Congratulations!") : _react.default.createElement(_core.Typography, {
    variant: "h6",
    color: "textSecondary"
  }, "Keep Studying!"), _react.default.createElement(_core.Box, {
    px: 2
  }, result ? _react.default.createElement(_core.Typography, {
    variant: "subtitle2",
    color: "textSecondary",
    component: "span"
  }, "You have completed the quiz successfully") : _react.default.createElement(_core.Typography, {
    variant: "subtitle2",
    color: "textSecondary",
    component: "span"
  }, "You haven't completed the quiz yet! Please try Again")))), _react.default.createElement(StyledCardActions, null, _react.default.createElement(_core.Box, {
    textAlign: "center",
    width: 1
  }, _react.default.createElement(_core.Typography, null, _react.default.createElement(_core.Link, {
    onClick: reTake
  }, "Retake Quiz"), review ? _react.default.createElement(ReviewQuiz, null) : ''))))), _react.default.createElement(ButtonBox, {
    px: 4,
    justifySelf: "flex-end"
  }, props.nextResource && props.next && _react.default.createElement(_FullWidthButton.default, {
    variant: "contained",
    color: "secondary",
    typovariant: "h5",
    padding: 2,
    onClick: function onClick() {
      return nextTest(props);
    }
  }, _react.default.createElement(_core.Typography, null, "Next"), _react.default.createElement(_core.Typography, null, "1.2 ", props.nextResource.name))));
}

var _default = TestSummaryPage;
exports.default = _default;