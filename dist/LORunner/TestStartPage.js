"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _core = require("@material-ui/core");

var _styledComponents = _interopRequireDefault(require("styles/styled-components"));

var _FullWidthButton = _interopRequireDefault(require("components/Button/FullWidthButton"));

var _Quiz_Begin = _interopRequireDefault(require("../assets/Quiz_Begin.svg"));

var _icons = require("@material-ui/icons");

var _Test = _interopRequireDefault(require("./Test.messages"));

var _reactIntl = require("react-intl");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n    background-size: 100% 100%;\n    background-image: url(", ");\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var ImageDiv = (0, _styledComponents.default)(_core.Box)(_templateObject(), _Quiz_Begin.default);

function TestStartPage(props) {
  var instructions = [_Test.default.instruction1, _Test.default.instruction2, _Test.default.instruction3];
  var buttonText = null;

  if (props.fetchedState && props.fetchedState.status === 'InProgress') {
    buttonText = _Test.default.resumeButton;
  } else {
    buttonText = _Test.default.startButton;
  }

  return _react.default.createElement(_core.Box, {
    height: "inherit",
    display: "flex",
    flexDirection: "column"
  }, _react.default.createElement(_core.Box, {
    flexGrow: 1
  }, _react.default.createElement(ImageDiv, {
    position: "relative",
    height: "45%",
    minHeight: "45%"
  }), _react.default.createElement(_core.Box, {
    overflow: "auto",
    maxHeight: "30%",
    height: "30%",
    m: 2
  }, instructions && instructions.map(function (instruction, index) {
    return _react.default.createElement(_core.Box, {
      display: "flex",
      key: index,
      py: 1
    }, _react.default.createElement(_core.Box, {
      pr: 1
    }, _react.default.createElement(_icons.ArrowForward, null)), _react.default.createElement(_core.Box, null, _react.default.createElement(_core.Typography, null, _react.default.createElement(_reactIntl.FormattedMessage, instruction))));
  })), _react.default.createElement(_core.Box, {
    px: 4
  }, _react.default.createElement(_FullWidthButton.default, {
    variant: "contained",
    color: "secondary",
    typovariant: "h5",
    padding: 2,
    onClick: function onClick() {
      return props.startTest();
    }
  }, _react.default.createElement(_reactIntl.FormattedMessage, buttonText)))));
}

var _default = TestStartPage;
exports.default = _default;