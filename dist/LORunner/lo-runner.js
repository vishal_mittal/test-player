"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireWildcard(require("react"));

var _core = require("@material-ui/core");

var _xapirunner = require("../libs/libs-frontend-xapi-3/dist/js/xapirunner");

var _styledComponents = _interopRequireDefault(require("styled-components"));

var _testService = require("../Services/testService");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _templateObject() {
  var data = _taggedTemplateLiteral(["\n& > iframe{\n    border:0;\n}\n"]);

  _templateObject = function _templateObject() {
    return data;
  };

  return data;
}

function _taggedTemplateLiteral(strings, raw) { if (!raw) { raw = strings.slice(0); } return Object.freeze(Object.defineProperties(strings, { raw: { value: Object.freeze(raw) } })); }

var StyledBox = (0, _styledComponents.default)(_core.Box)(_templateObject());
var LORunner = (0, _react.forwardRef)(function (props, ref) {
  var uniqueLOId = '1519132651151/1519132667943'; // Unique LO Id.

  var _useState = (0, _react.useState)({
    xapiLO1: _xapirunner.XAPIRunner
  }),
      _useState2 = _slicedToArray(_useState, 2),
      state = _useState2[0],
      setState = _useState2[1];

  (0, _react.useEffect)(function () {
    (0, _testService.getItemState)(props.itemCode).then(function (state) {
      if (state) {
        launchLO(props.url, state);
      } else {
        launchLO(props.url, {});
      }
    }).catch(function (e) {
      return console.log(e);
    });
  }, []);
  (0, _react.useImperativeHandle)(ref, function () {
    return callAction;
  });

  function callAction(action) {
    state.xapiLO1.containerInvocation(uniqueLOId, "generic_controls", {
      type: action
    });
  }

  function launchLO(url, currState) {
    //Setup initialization params.
    var initParams = {
      "paths": {
        "dependencyBase": window.location.origin + "/libs",
        //FQDN path for xAPIRunner dependencies.
        "contentAssetBase": "./" //FQDN path for contentAssest dependencies.

      },
      "userId": "",
      //Unique User ID
      "productId": "",
      //Unique Product ID
      "classId": "" //Unique Class ID

    };
    var itemParams = {
      'learningObject': {
        'path': url,
        //   'path': 'https://cdn1.comprodls.com/cosmatt1-qa/products/biology_and_geology/1/assets/LO-Test4',
        'contentType': 'ext-cup-xapiscoreable',
        'code': '1519132851151',
        //Item-code
        'name': 'IC5_OWB_L0_U01_Ex01',
        //Optional.
        'status': '' //String - not_started/in_complete/completed

      },
      //Optionally specify container configurations.
      'container': {
        'type': "generic-content",
        //Possible types - generic-content
        'config': {
          'disableLOButtons': true //Boolean. Default value - false.

        }
      }
    }; //Setup event handlers for receiving events from LO.

    var eventCallbacks = {
      'newState': function newState(uniqueId, _newState) {
        props.currentState.setFetchedState(_newState);
        (0, _testService.setItemState)(props.itemCode, _newState).then(function (data) {// console.log(data)
        }).catch(function (e) {
          return console.log(e);
        }); // console.log('new state received');
      },
      'newStatements': function newStatements(uniqueId, _newStatements) {
        //   console.log('new statement received');
        if (_newStatements && _newStatements.length) {
          var event = _newStatements[0].json.verb.display.und;

          if (event == 'launched') {
            callAction('currentScreen');
            callAction('totalScreens');
          } else if (event == 'started') {
            props.ready();
          }
        }
      },
      'newInterpretedStatements': function newInterpretedStatements(uniqueId, _newInterpretedStatements, newInterpretedVerbs) {//   console.log('new interpreted statements received');
      },
      'newDimension': function newDimension(uniqueId, dimensions) {//   console.log('new dimension received');
      },
      'containerNotification': function containerNotification(uniqueId, containerEvent, containerEventData) {
        if (containerEvent === 'generic_controls') {
          props.updateBtnState(containerEventData);
        }
      },
      messageFromContent: function messageFromContent(uniqueId, newMessageFromContent) {
        // console.log("message from content: ", uniqueId, newMessageFromContent);
        if (newMessageFromContent && newMessageFromContent.length) {
          var containerEvent = newMessageFromContent[0].request.type;
          var containerEventData = newMessageFromContent[0].request.data;

          if (containerEvent === 'showResult') {
            props.showResult(containerEventData.score, containerEventData.review); // console.log('showResult received');
          }
        }
      },
      messageToContent: function messageToContent(uniqueId, newMessageToContent) {
        // console.log("message to content: ", uniqueId, newMessageToContent);
        if (newMessageToContent && newMessageToContent.length) {
          var containerEvent = newMessageToContent[0].request.type;
          var containerEventData = newMessageToContent[0].response;

          if (containerEvent === 'currentScreen') {
            props.updateQuestionNumbers(containerEventData, undefined);
          } else if (containerEvent === 'totalScreens') {
            props.updateQuestionNumbers(undefined, containerEventData);
          } else if (containerEvent === 'sendScores') {// console.log(containerEventData);
          }
        }
      }
    };
    var containerElementRef = 'container-lo'; //Unique Container Id.
    // Initialize XAPIRunner Instance & setup event handlers.

    state.xapiLO1 = new _xapirunner.XAPIRunner(initParams, eventCallbacks, function () {//    console.log("XapiRunner Callback Function ")
    }); // Initialize and paint the activity.

    state.xapiLO1.paintActivity(uniqueLOId, containerElementRef, itemParams, {
      id: 1519132851151,
      loWithoutControls: true,
      state: Object.keys(currState).length > 0 ? currState : null
    }); //  setState({...state});
  }

  return _react.default.createElement(StyledBox, {
    height: 1,
    id: 'container-lo'
  });
});
var _default = LORunner;
exports.default = _default;