"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getItemState = exports.setItemState = void 0;

var _service = require("./service");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var setItemState =
/*#__PURE__*/
function () {
  var _ref = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee(itemCode, state) {
    var config;
    return regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            config = {
              method: 'post',
              url: '/api/user/product/progress/state/123/' + encodeURIComponent(itemCode),
              data: {
                key: state
              }
            };
            return _context.abrupt("return", (0, _service.resolveRequest)(config).then(function (response) {
              return response.data;
            }));

          case 2:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function setItemState(_x, _x2) {
    return _ref.apply(this, arguments);
  };
}();

exports.setItemState = setItemState;

var getItemState =
/*#__PURE__*/
function () {
  var _ref2 = _asyncToGenerator(
  /*#__PURE__*/
  regeneratorRuntime.mark(function _callee2(itemCode) {
    var config;
    return regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            config = {
              url: '/api/user/product/progress/state/123/' + encodeURIComponent(itemCode)
            };
            return _context2.abrupt("return", (0, _service.resolveRequest)(config).then(function (response) {
              return response.data.key;
            }));

          case 2:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));

  return function getItemState(_x3) {
    return _ref2.apply(this, arguments);
  };
}();

exports.getItemState = getItemState;