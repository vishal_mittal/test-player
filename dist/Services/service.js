"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resolveRequest = void 0;

var axios = require('axios');

axios.defaults.baseURL = 'https://comprodls-engage-backend.herokuapp.com/';

var resolveRequest = function resolveRequest(config) {
  try {
    var response = axios.request(config);
    return response;
  } catch (error) {
    console.log(error);
  }
};

exports.resolveRequest = resolveRequest;