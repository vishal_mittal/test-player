# libs-frontend-xapi | Launching & Rendering 
Library for implementing a **Container managed** approach for launching, embedding and interacting (data capture) with a xAPI / TinCan compliant learning object. Applications (experience apps) should use this library for setting up a **container** to establish two-way communication channel with an embedded (iframe) LO.

**xAPI LO | Packaging Structure:**

This is assumed to be an externally created/authored package/archive with the following structure. The **index.html** file is a launch point for package **<root>** - which is responsible for automatically rendering/playing the learning object with the following rules & conventions. The **<assets>** folder is optional and typically used for local assets of the LO.

```
<root>
     <assets>
     index.html
```

**xAPI LO | Javascript Interface & Launch Handshake:**

![Container Managed Handshake](doc/container_managed_handshake.png?raw=true "Container Managed Handshake")

The LO (index.html) is responsible for implementing a Javascript interface/handshake for establishing a communication channel with the container. The container (via xAPIRunner) creates the iframe that hosts the LO and loads the index.html. After that the container **waits** for the LO to self initialize and then eventually call ```parent.LOReady```. This informs the container that LO is READY to receive the **adaptor** object. Adaptor is a proxy for the container, and is used by the LO to communicate with the container.

```javascript
//index.html, LO code

/* Startup Logic */
..
..
var containerAdaptor;
parent.LOReady( function(adaptor) {
   /* Save the Adaptor for future communication with the Container */
   containerAdaptor = adaptor;
})
..
..


/* Intialization & Rendering Logic */
initParams = containerAdaptor.getInitParams();
..
..
containerAdaptor.newDimension(id, width, height)


/* User Interaction Logic */
..
..
containerAdaptor.newStatements(id, .....)

```

**Responsibilities of the LO:**
- Must call ```adaptor.newDimension``` event after fully rendering itself.
- Must call ```adaptor.newStatements``` whenever new statement/data is available.
- Must call ```adaptor.newState``` whenever its internal state changes.

## 1. Adding xAPI Runner to your project
Use NPM to install xAPI Runner.
```bash
npm install libs-frontend-xapi
```
**Additional Dependencies**
* [JQuery](https://github.com/jquery/jquery) version 3.2.1 or higher.

## 2. Usage
Include xapiRunner in your project (HTML) file as shown below & initialize with appropriate parameters to render your Learning Object or LO.

```html
<html>
  <head>
    <script src="../dist/js/xapirunner-min-0.4.9.js" type="text/javascript" charset="utf-8"></script>
  </head>
  <body>
    <div id="containerLO1"/>
    
    <script language="javascript" type="text/javascript"> 
      //Setup initialization params.
      var initParams = {  
        'paths': {  "dependencyBase" : "https://.../bower_components/", //FQDN path for xAPIRunner dependencies.
                    "contentAssetBase": "https://" //FQDN path for contentAssest dependencies.
         } 
        'userId': '', //Unique User ID
        'productId': '', //Unique Product ID
        'classId': '' //Unique Class ID
      };

      var itemParams = {  
          'learningObject': { 'path': 'https://s3.amazonaws.com/cup-content/IC5_OWB_L0_U01_Ex01',
                              'contentType' : 'ext-cup-xapiscoreable',
                              //Supported types - ext-cup-xapiscoreable
                              'code' : '1519132851151', //Item-code
                              'name': 'IC5_OWB_L0_U01_Ex01', //Optional.
                              'state' : "...........", //Optional state. Use this to Resume previous activity session.
                              'status': '' //String - not_started/in_complete/completed
           },
          //Optionally specify container configurations.
          'container': {
               'type': "generic-content", //Possible types - generic-content
               'config': {
                  'disableLOButtons': false //Boolean. Default value - false.
                }
          }
      };

      //Setup event handlers for receiving events from LO.
      var eventCallbacks = {                
          'newState': function(uniqueId, newState) {/* Do Something */},
          'newStatements': function(uniqueId, newStatements) {/* Do Something */},
          'newInterpretedStatements': function(uniqueId, newInterpretedStatements, newInterpretedVerbs) {/* Do Something */},
          'newDimension': function(uniqueId, dimensions) {/* Do Something */}
          //Optional container specific notifications, unique to very container type.
//           'containerNotification': function(uniqueId, containerEvent, containerEventData) {
               //Example code for container.type = 'generic-content'
              //  if (containerEvent === 'generic_controls' {
              //     console.log(containerEventData.buttonType);
              //     console.log(containerEventData.state);
              //     console.log(containerEventData.action);
              //  }
           }
      };     
  
     var uniqueLOId = '1519132651151/1519132667943'; // Unique LO Id.
     var containerElementRef = 'containerLO1'; //Unique Container Id.

     // Initialize XAPIRunner Instance & setup event handlers.
     var xapiLO1 = new XAPIRunner(initParams, eventCallbacks, callback);
     // Initialize and paint the activity.
     xapiLO1.paintActivity(uniqueLOId, containerElementRef, itemParams);
  
     // Call Methods to interact with the LO.
     xapiLO1.reset(uniqueLOId, state);
    </script> 
  </body>
</html>  
  
```

## 3. Events
Events are raised by the LO and can be captured or handled by the container. In order to handle events, the container must setup corresponding event handlers during initialization.

### 3.1 newState
Internal state of the LO has changed. Note, it is the responsibility of the application/container to save the state by handling this event. This is typically used when RESUMING an in-progress activity via the ```reset(state)``` method.

**Parameters** 
* **id**: Unique Activity Id (used to determine the event generated is corresponding to which activity).
* **state**: String (any format, managed by the LO)

### 3.2 newStatements
One or more new statements have been generated by LO 

**Parameters** 
* **id**: Unique Activity Id (used to determine the event generated is corresponding to which activity).
* **statements**: Array of Statements (JSON)

### 3.3 newInterpretedStatements
One or more new interpreted statements have been generated either by LO or by Container or based on some Business rules. 

**Parameters** 
* **id**: Unique Activity Id (used to determine the event generated is corresponding to which activity).
* **statements**: Array of Interpreted Statements (JSON)
* **verbs**: Array of Verbs in Interpreted Statements

### 3.4 containerNotification 
These are custom events generated by the specific container implementations or types.

**Parameters** 
* **id**: Unique Activity Id (used to determine the event generated is corresponding to which activity).
* **containerEvent**: Event being triggered - generic_controls, container_controls, iframeEvents and size.
* **containerEventData**: Event data being triggered.

#### containerEventData corresponding to different containerEvent :

#### Content custom notifications - 'generic_controls'

**controlsChange**
This event is generated by LOs, informing the container which button to show and when. If state is true, show the button specified in buttonType and vice-versa.

*EventData*
* buttonType: String - Possible values - checkAnswer, goNext, sendScores, goToExerciseScreen. Also event activityClosed is raised when                                            the LO is successfully closed.
* state: Boolean - Whether to show button or hide. If true call respective funtion of xapirunner on click.

**loaded**
This event is generated by the LOs, informing the container that its loaded and ready for interaction by the end-user. The container can use this event to show appropriate **Loading** behavior.

*EventData**
* *id*: Unique Activity Id.

#### Size notifications - 'size'

**size**
This event is generated by the LOs, informing the container that its internal dimension (rendering size) has changed. The container can handle this event to resize the iframe and avoid scrolling.

*EventData*
* width: String 
* height: String

#### Iframe Events notifications - 'iframeEvents'

**iframeEvents**
This event is generated by the container when some event has been captured.

*EventData*
* event: String (click/touchstart)

#### Container notifications - 'container_controls'

**containerControls**
This event is generated by the container when activity is completed (APP can use this event to show next button to navigate from one LO to another).

*EventData*
* event: String (activityCompleted)

## 4. Methods
Methods used by the container to communicate with the LO.

### 4.1 reset
Repaint or Re-initialize an existing xAPI activity or LO

**Parameters** 
* **state**: Optional String (any format, managed by the LO) 

### 4.2 containerInvocation
This method allow you to invoke the custom methods implemented by specific container.

**Parameters** 
* **id**: Unique Activity Id (used to determine the event generated is corresponding to which activity).
* **containerMethod**: String - Name of the specific container method. For example 'generic_controls' custom method.
* **containerMethodData**: HashMap of custom parameters. For example generic_controls method has parameters - containerMethodData.action (Possible values - checkAnswer, goNext)

### 4.5 generateCustomStatement
Generate new custom statement for a particular learning object.

**Parameters** 
* **id**: Unique Activity Id.
* **verb**: Verb for the new statement to be posted (launched/submitted/closed/downloaded/evaluated).
* **options**: JSON object consisting - 'text' and 'audioPath' to be added in the 'submitted' statement object
                                      - 'student_userid', 'link-statementid' 'score' and 'comment' (optional) to be added in the
                                        'evaluated' statement  object. (score : { "scaled": <float>, "min": <float>, "raw": <float>,
                                         "max": <float> })
