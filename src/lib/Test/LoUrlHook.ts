export function useLoUrl(props){
    // let routeStateData = props.history.location.state;
    if(!(props && props.resource)){
        return {}
    }
    let item = props.resource;
    let type = item.subType;
    let url = item[type].url;
    let questionCount = item[type].cupOptions.questionCount;
    let totalScore = item[type].cupOptions.totalScore;
    let passScore = item.passScore;

    return {
            url:props.baseUrl+"/"+url,
            resourceId:item.resource,
            questionCount,
            totalScore,
            passScore,
            itemCode:item.itemCode
            }
  }