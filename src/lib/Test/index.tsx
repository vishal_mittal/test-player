import React, { useEffect } from 'react';
import PrivateRoute from 'PrivateRoute';
import TestRunner from '../LORunner/test-runner';
import { Route, Redirect } from 'react-router';
import TestStartPage from '../LORunner/TestStartPage';
import TestSummaryPage from '../LORunner/TestSummaryPage';
import { useLoUrl } from './LoUrlHook';
import { setItemState } from '../Services/testService';
import { useItemState } from './loItem.hook';

function Test(props: any) {
  let status = useLoUrl(props)
  const currentState = useItemState(status.itemCode)
  function startTest(){
    if(currentState && currentState.fetchedState && currentState.fetchedState.status!=='InProgress'){
            currentState.setFetchedState({});
            setItemState(status.itemCode,{}).then(succ=>{
                props.history.push({
                    pathname: props.location.pathname + "/" + "on",
                    state: {
                        url:status.url,
                        resourceId:status.resourceId,
                        totalScore:status.totalScore,
                        passScore:status.passScore,
                        itemCode:status.itemCode
                    }
                },)
            }).catch(err=>console.log(err))
    }else{
        props.history.push({
            pathname: props.location.pathname + "/" + "on",
            state: {
                url:status.url,
                resourceId:status.resourceId,
                totalScore:status.totalScore,
                passScore:status.passScore,
                itemCode:status.itemCode
            }
        },)
    }
}
function showResult(score:any,review:any){
  props.history.push({
    pathname: props.location.pathname + "/" + "summary",
    state:{
        score:score,
        review:review,
        resourceId:status.resourceId,
        totalScore:status.totalScore,
        passScore:status.passScore,
        itemCode:status.itemCode
    }
});
}

  const sendPageStatus = (page) => {
    if(props.on) {
      props.on(page);
    }
  }

  return (
    <>
    {props.baseUrl && props.resource && <>
    <Route exact path={`${props.match.path}/on/summary`} render={()=>{
      sendPageStatus('summary');
      return <TestSummaryPage currentState={currentState} next={props.next} {...props}/>
    }}/>
    <Route exact path={`${props.match.path}/on`} render={()=>{
      sendPageStatus('progress');  
      return <TestRunner currentState={currentState} showResult={showResult} {...props}/>
    }}/>
    <Route exact path={`${props.match.path}`} render={()=>{
      sendPageStatus('start');  
      return <TestStartPage fetchedState={currentState.fetchedState} startTest={startTest} status={status} {...props}/>
    }}/>
    </>}
    </>
  )
}

export default Test;
