import { useState, useEffect } from "react";
import { getItemState } from "../Services/testService";
export function useItemState(itemCode){
    const [fetchedState,setFetchedState]=useState()
    useEffect(() => {
    getItemState(itemCode).then(state=>{
        setFetchedState(state)
    }).catch(e=>console.log(e))
    
    },[]);
    return {fetchedState,setFetchedState}
}

