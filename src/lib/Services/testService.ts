import { resolveRequest } from "./service";

export const setItemState = async (itemCode:string,state:any)=>{
    const config={
        method: 'post',
        url : '/api/user/product/progress/state/123/'+encodeURIComponent(itemCode),
        data : {key:state}
    }
    return resolveRequest(config).then((response) => response.data);
}

export const getItemState = async (itemCode:string)=>{
    const config={
        url : '/api/user/product/progress/state/123/'+encodeURIComponent(itemCode),
    }
    return resolveRequest(config).then((response) => response.data.key);
}
