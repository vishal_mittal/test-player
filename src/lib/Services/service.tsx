
const axios=require('axios')
axios.defaults.baseURL = 'https://comprodls-engage-backend.herokuapp.com/';

export const resolveRequest:Function=(config:any)=>{
    try{
        const response = axios.request(config)
        return response
    }
    catch(error){
        console.log(error)
    }
}
