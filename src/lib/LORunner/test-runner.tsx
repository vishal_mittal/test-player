import React, { useState, useEffect } from 'react';
import { Box, Typography, Theme, Button, IconButton, Icon, Divider} from '@material-ui/core';
import {createStyles, makeStyles, withStyles} from '@material-ui/styles';
import LoRunner from './lo-runner';
import FullWidthButton from 'components/Button/FullWidthButton';
import CloseIcon from '@material-ui/icons/Close';
import styled from 'styled-components';
import { FormattedMessage } from 'react-intl';

import messages from './Test.messages';
import ErrorBoundary from 'containers/ErrorBoundary';

const styles = (theme: any) => ({
    upToSmall: {
        [theme.breakpoints.down('sm')]: {
            display: 'none',
        },
    },
    aboveSmall: {
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    paddingAdjustment: {

        [theme.breakpoints.up('md')]: {
            paddingBottom: theme.spacing(2),
        },
    },
});

function QuestionNumber(props) {
    return (
        <Box fontWeight={600} flexGrow={1} pl={3} pr={0.5} pt={2} pb={1}>
            <Typography color="inherit" variant="h6">
            <FormattedMessage {...messages.question}/>&nbsp;
            {props.current}/{props.total}</Typography>
        </Box>
    );
}
const CheckWorkButton=styled(Button)`
padding:${props=>props.theme.spacing(0.5,1)};
background-color:${props=>props.theme.palette.secondary.main};
color:white;
box-shadow:${props=>props.theme.shadows[4]};
font-weight:500;
width:50%;
&:disabled{
    // background-color:rgba(255,255,255,0.5);
    color:white;
    opacity:0.5;
    box-shadow:none;
}
// &:hover:disabled{
//     background-color:rgba(255,255,255,0.5);
// }
`
const NextButton =styled(CheckWorkButton)`
&:hover{
    background-color:${props=>props.theme.palette.secondary.main};
}
`
const StyledButton=styled(Button)`
color:white;
&:disabled{
    color:#616161;
}
`
const NavigationBox=styled(Box)`
color:${props=>props.theme.tertiary.main};
`
const TestFooter=styled(Box)`
background-color:#f2f2f2;
`
function TestRunner(props: any) {
    const { classes } = props;
    let loRunnerRef: any = React.useRef();

    let resourceId = props.location.state.resourceId;
    let totalScore = props.location.state.totalScore;
    let passScore = props.location.state.passScore;
    let itemCode =  props.location.state.itemCode;
    let currState = {};
    let isReview=false;
    if(props.currentState && props.currentState.fetchedState && props.currentState.fetchedState.status==='Completed'){
        isReview = true
    }
    

    let [quesNo,setQuesNo]=useState({
        currentQNo: 0,
        totalQNo: 0
    })
    let url = props.location.state.url;
    let [state, setState] = useState({
        isNext: true, 
        isPrev: false,
        isCheckAnswer: false, 
        isHint: false,
        isTryAgain: false,
        
    });
    let [testReady, setTestReady] = useState(false);

    function updateBtnState(newState) {
        if(newState.control === "goNext" ){
            state.isNext = newState.visible;
        } else if(newState.control === "goPrev"){
            state.isPrev = newState.visible;
        } else if(newState.control === 'checkAnswers'){
            state.isCheckAnswer = newState.visible;
        } else if (newState.hint === 'hint'){
            state.isHint = newState.visible;
        } else if (newState.control === 'tryAgain'){
            state.isTryAgain = newState.visible;
        }
        setState({...state});      
    }

    function updateQuestionNumbers(currQues,totQues){
        if(currQues != undefined){
            quesNo.currentQNo = currQues;
            
        }else if(totQues != undefined){
            quesNo.totalQNo = totQues;
        }
        setQuesNo({...quesNo});      
    }

    function onBtnClick(btn) {
        if(btn === 'next') {
            loRunnerRef.current('goNext');
        } else if(btn === 'previous') {
            loRunnerRef.current('goPrev');
        }else if(btn === 'checkAnswers'){
            loRunnerRef.current('checkAnswers');
        }else if(btn==='Hint'){
        } else if (btn === 'sendScores'){
            // sessionStorage.removeItem(resourceId);
            loRunnerRef.current('sendScores');
        } else if (btn === 'tryAgain'){
            loRunnerRef.current('tryAgain');
        }
    }

    function onTestReady() {
        setTestReady(true);
    }
    function closePlayer(){
        // let newState = props.currentState.fetchedState
        // newState.status='InProgress'
        // isReview?null:props.currentState.setFetchedState(newState)
        props.history.goBack();
    }
    function goToSummary(){
        props.history.push({
            pathname: props.location.pathname + "/" + "summary",
            state:{
                review:true,
                resourceId:resourceId,
                totalScore:totalScore,
                passScore:passScore,
            }
        });
    }
    return (
    <Box height={1} overflow="auto" flexDirection="column" display='flex'>
        {testReady &&
        <Box textAlign="left" display='flex' flexDirection="row" alignItems="center" bgcolor="primary.main" color='primary.contrastText'>
            <QuestionNumber current={quesNo.currentQNo} total={quesNo.totalQNo}></QuestionNumber>
            <IconButton onClick={closePlayer} color='inherit'>
                <CloseIcon></CloseIcon>
            </IconButton>
        </Box>}
        
        <Box flexGrow={1}>
            <ErrorBoundary>
            <LoRunner ref={loRunnerRef} ready={onTestReady} itemCode={itemCode} resourceId={resourceId} totalScore={totalScore} passScore={passScore} currState={currState} updateQuestionNumbers={updateQuestionNumbers} updateBtnState={updateBtnState} url={url} {...props}></LoRunner>
            </ErrorBoundary>
        </Box>
        {testReady &&
        <TestFooter p={2} pb={0} textAlign="center" bgcolor="primary.main">
               {
                    !state.isTryAgain && !isReview? 
                    <CheckWorkButton disabled={!state.isCheckAnswer} onClick={() => {onBtnClick('checkAnswers')}}>
                        <Typography variant='body1'><FormattedMessage {...messages.checkWorkButton}/></Typography>
                    </CheckWorkButton>:
                    <NextButton onClick={() => {
                                state.isNext? onBtnClick('next'):
                                (isReview?goToSummary():onBtnClick('sendScores'))
                                }}>
                        <Typography variant='body1'>{state.isNext?'Next Question':(isReview?'Close Review':'Finish')}
                        </Typography>
                    </NextButton>
                }
                <NavigationBox pt={1}>
                    <Button component="span" variant="text" color="inherit"  
                        disabled={!state.isPrev} onClick={() => onBtnClick('previous')}>
                            Previous
                    </Button>
                    <Typography component='span' color='textSecondary'>|</Typography>
                    {/* <Button component="span" variant="text" color="inherit"
                        disabled={(!state.isNext && !state.isTryAgain) || isReview}
                        onClick={() => {!state.isTryAgain?onBtnClick('next'):onBtnClick('tryAgain')}}>
                        {!state.isTryAgain && !isReview?"Next":'Try Again'}
                    </Button> */}
                    <Button component="span" variant="text" color="inherit"
                        disabled={isReview}
                        onClick={() => {state.isTryAgain?onBtnClick('tryAgain'):(state.isNext?onBtnClick('next'):onBtnClick('sendScores'))}}>
                        {(state.isTryAgain || isReview)?'Try Again':(!state.isNext?'Finish':'Next')}
                    </Button>
                </NavigationBox>
        </TestFooter>}
    </Box>
    );
}

export default withStyles(styles)(TestRunner);
