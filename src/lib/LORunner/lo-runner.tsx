import React, { useState, createRef, useEffect, useImperativeHandle, forwardRef } from 'react';
import { Box } from "@material-ui/core";

import {XAPIRunner} from '../libs/libs-frontend-xapi-3/dist/js/xapirunner';
import styled from 'styled-components';
import { setItemState, getItemState } from '../Services/testService';
const StyledBox=styled(Box)`
& > iframe{
    border:0;
}
`
const LORunner = forwardRef((props: any, ref) => {
    let uniqueLOId = '1519132651151/1519132667943'; // Unique LO Id.
    const [state, setState] = useState({xapiLO1:XAPIRunner});
    useEffect(() => {
        getItemState(props.itemCode).then(state=>{
            if(state){
                launchLO(props.url,state);
            }else{
                launchLO(props.url,{});  
            }
            
        }).catch(e=>console.log(e))
        
    },[]);

    useImperativeHandle(ref, () => (callAction));

    function callAction(action:string){
        state.xapiLO1.containerInvocation(uniqueLOId,"generic_controls",{type:action});  
    }

    function launchLO(url,currState) {
        //Setup initialization params.
        var initParams = {
            "paths": {
                "dependencyBase" : window.location.origin + "/libs", //FQDN path for xAPIRunner dependencies.
                "contentAssetBase": "./" //FQDN path for contentAssest dependencies.
             },
            "userId": "", //Unique User ID
            "productId": "", //Unique Product ID
            "classId": "" //Unique Class ID
          };

          var itemParams = {
              'learningObject': {
                 'path': url,
                //   'path': 'https://cdn1.comprodls.com/cosmatt1-qa/products/biology_and_geology/1/assets/LO-Test4',
                  'contentType' : 'ext-cup-xapiscoreable',
                  'code' : '1519132851151', //Item-code
                  'name': 'IC5_OWB_L0_U01_Ex01', //Optional.
                  'status': '' //String - not_started/in_complete/completed
               },
              //Optionally specify container configurations.
              'container': {
                   'type': "generic-content", //Possible types - generic-content
                   'config': {
                      'disableLOButtons': true //Boolean. Default value - false.
                    }
              }
          };

          //Setup event handlers for receiving events from LO.
          var eventCallbacks = {
              'newState': function(uniqueId, newState) {
                  props.currentState.setFetchedState(newState)
                  setItemState(props.itemCode,newState).then((data)=>{
                    // console.log(data)
                  }).catch(e=>console.log(e))
                // console.log('new state received');
              },
              'newStatements': function(uniqueId, newStatements) {
                //   console.log('new statement received');
                  if(newStatements && newStatements.length){
                      let event = newStatements[0].json.verb.display.und;
                      if(event=='launched'){
                        callAction('currentScreen')
                        callAction('totalScreens')
                      }
                      else if(event=='started'){
                        props.ready();
                      }
                  }
              },
              'newInterpretedStatements': function(uniqueId, newInterpretedStatements, newInterpretedVerbs) {
                //   console.log('new interpreted statements received');
              },
              'newDimension': function(uniqueId, dimensions) {
                //   console.log('new dimension received');
              },
              'containerNotification': function(uniqueId, containerEvent, containerEventData) {
                   if (containerEvent === 'generic_controls') {
                    props.updateBtnState(containerEventData);
                   }
               },
              messageFromContent: function(uniqueId, newMessageFromContent) {
                // console.log("message from content: ", uniqueId, newMessageFromContent);
                if(newMessageFromContent && newMessageFromContent.length) {
                    let containerEvent = newMessageFromContent[0].request.type;
                    let containerEventData = newMessageFromContent[0].request.data;
                    if(containerEvent === 'showResult') {
                        props.showResult(containerEventData.score,containerEventData.review)
                        // console.log('showResult received');
                    }
                }
              },
              messageToContent: function(uniqueId, newMessageToContent) {
                // console.log("message to content: ", uniqueId, newMessageToContent);
                if(newMessageToContent && newMessageToContent.length) {
                    let containerEvent = newMessageToContent[0].request.type;
                    let containerEventData = newMessageToContent[0].response;
                    if(containerEvent === 'currentScreen') {
                        props.updateQuestionNumbers(containerEventData,undefined);                        
                    } else if(containerEvent === 'totalScreens') {
                        props.updateQuestionNumbers(undefined,containerEventData);
                    } else if(containerEvent === 'sendScores') {
                        // console.log(containerEventData);
                    }
                }
              }
          };

         var containerElementRef = 'container-lo'; //Unique Container Id.

         // Initialize XAPIRunner Instance & setup event handlers.
         state.xapiLO1 = new XAPIRunner(initParams, eventCallbacks, function(){
        //    console.log("XapiRunner Callback Function ")
         });
         // Initialize and paint the activity.
         state.xapiLO1.paintActivity(uniqueLOId, containerElementRef, itemParams, {id:1519132851151,loWithoutControls:true,state:Object.keys(currState).length>0?currState:null});
        //  setState({...state});


    }
    return (
        <StyledBox height={1} id={'container-lo'}></StyledBox>
    );
});

export default LORunner;
