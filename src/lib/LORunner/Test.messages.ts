/*
 * PracticePage Messages
 *
 * This contains all the text for the PracticePage component.
 */
import { defineMessages } from 'react-intl';

export const scope = 'boilerplate.containers.Test';

export default defineMessages({
  heading:{
    id: `${scope}.teststart.listdata.heading`,
    defaultMessage: 'Start Page List Data',
  },
  instruction1:{
    id: `${scope}.teststart.listdata.message1`,
    defaultMessage: 'Start Page List Data',
  },
  instruction2:{
    id: `${scope}.teststart.listdata.message2`,
    defaultMessage: 'Start Page List Data',
  },
  instruction3:{
    id: `${scope}.teststart.listdata.message3`,
    defaultMessage: 'Start Page List Data',
  },
  startButton:{
    id: `${scope}.teststart.startbutton.message`,
    defaultMessage: 'START QUIZ',
  },
  resumeButton:{
    id: `${scope}.teststart.resumebutton.message`,
    defaultMessage: 'RESUME QUIZ NOW',
  },
  question:{
    id: `${scope}.teston.question.message`,
    defaultMessage: 'Question',
  },
  checkWorkButton:{
    id: `${scope}.teston.checkworkbutton.message`,
    defaultMessage: 'CHECK MY WORK',
  },
});
