import React from 'react'
import { Box, Paper, Card, CardContent, Typography, CardActions, Button, IconButton, Theme, createStyles, Link } from '@material-ui/core'
import FullWidthButton from 'components/Button/FullWidthButton'
import CloseIcon from '@material-ui/icons/Close';
import { makeStyles } from '@material-ui/styles';
import styled from 'styled-components';
import { setItemState } from '../Services/testService';
import Background from '../assets/Trophy.svg';
import TryAgainImage from '../assets/TryAgain.svg';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        borderedCard:{
            marginTop:'45px',
            flexGrow:1
        },
        roundedDiv:{
            'background-color': `${theme.palette.primary.main}`,
            'height': '100px',
            'width': '100px',
            'border-radius': '50%',
            'align-items': 'center',
            'display': 'flex',
            'justify-content': 'center',
            'position': 'absolute',
            'top':'0',
            'left': 'calc(50% - 50px)',
            'color':`${theme.palette.primary.contrastText}`
        },
        successText:{
            color: `${theme.palette.error.main}`
        }
    }),
);


const StyledBox=styled(Box)`
background-color:#86dafa;
`
const ScoreBox=styled(Box)`
box-shadow:${props=>props.theme.shadows[5]};
z-index:1;
`
const ButtonBox=styled(Box)`
bottom:0
`
const SuccessText=styled(Typography)`
color:#87990f;
`
const BoldText=styled(Typography)`
font-weight : 500;
`
const ImageDiv = styled(Box)`
    background-size: 100% 100%;
    background-image: url(${Background});
`
const StyledCard = styled(Card)`
    display:flex;
    flex-direction:column;
    position:relative;
    margin-top:50px;
    flex-grow:1;
    border-top: 5px solid #5674b9;
    padding:0px 24px;
`
const StyledCardActions=styled(CardActions)`
    padding:12px 0px;
`
const StyledCardContent=styled(CardContent)`
    padding:0px;
`
const TryAgainImageDiv = styled(Box)`
    background-size: 100% 100%;
    background-image: url(${TryAgainImage});
`
function TestSummaryPage(props:any) {

    let currentScore = props.location.state?props.location.state.score: 0;
    let review = props.location.state?props.location.state.review: false;
    let totalScore = props.location.state?props.location.state.totalScore: 7;
    let passScore = props.location.state?props.location.state.passScore: 0.33;
    let itemCode = props.location.state?props.location.state.itemCode: undefined;
    let result = currentScore > (passScore*totalScore)


    function nextTest(props){
        if(props.next) {
            props.next();
        }
    }

    function closePlayer(){
    }

    function reTake(){
        props.currentState.setFetchedState({});
        setItemState(itemCode,{}).then(succ=>{props.history.goBack()}).catch(e=>console.log(e))   
    }

    const classes = useStyles();
    function launchReviewMode(){
        props.history.goBack();
    }

    function ReviewQuiz(){
        return (
                <>
                &nbsp;|&nbsp;
                <Link  onClick={launchReviewMode} >
                    Review Quiz
                </Link>
                </>
                )
    }

    return (
        <StyledBox height='inherit' display='flex' flexDirection='column'>
            <Box display='flex' flexDirection='row-reverse'>
                <IconButton aria-label="close" onClick={closePlayer} >
                    <CloseIcon></CloseIcon>
                </IconButton>
            </Box>
            <Box fontWeight="fontWeightBold" textAlign='center' pb={1}>
                <BoldText variant="h5" >Your Score</BoldText>
            </Box>
            <Box  px={4} pb={2.5} minHeight='50%' height='60%' display='flex' flexDirection='column' position='relative'>
            <ScoreBox className = {classes.roundedDiv} fontWeight="fontWeightBold">
                            <Typography variant="h5">{currentScore}/{totalScore}</Typography>
                            </ScoreBox>
                    <StyledCard>
                           
                        <StyledCardContent  className = {classes.borderedCard}>
                         
                        <Box height='60%' width='100%'>
                        {result?
                        <ImageDiv position='relative' height='80%' width='100%'>
                        </ImageDiv>:
                        <TryAgainImageDiv position='relative' height='100%' width='100%'>
                        </TryAgainImageDiv>
                        }
                        </Box>
                        
                        <Box textAlign="center" height='40%'>
                            {result?
                                <SuccessText variant="h6">
                                Congratulations!
                                 </SuccessText>:
                                <Typography variant="h6" color='textSecondary'>
                                Keep Studying!
                                 </Typography>
                            }
                            <Box px={2}>
                            {result?
                            <Typography variant="subtitle2" color='textSecondary' component='span'>
                            You have completed the quiz successfully
                            </Typography>:
                            <Typography variant="subtitle2" color='textSecondary' component='span'>
                            You haven't completed the quiz yet! Please try Again
                            </Typography>
                            }
                            </Box>
                            
                            
                        </Box>
                        </StyledCardContent>
                        <StyledCardActions >
                            <Box textAlign="center" width={1}>
                                <Typography>
                                    <Link  onClick={reTake} >
                                        Retake Quiz
                                    </Link>
                                    {review?<ReviewQuiz></ReviewQuiz>:''}
                                </Typography>
                            </Box>
                            
                        </StyledCardActions>
                    </StyledCard>
            
            </Box>
            <ButtonBox px={4} justifySelf='flex-end' >
                {props.nextResource && props.next &&
                <FullWidthButton 
                variant="contained" 
                color="secondary" 
                typovariant="h5" 
                padding={2} 
                onClick={()=>nextTest(props)}>
                    <Typography>Next</Typography>
                    <Typography>1.2 {props.nextResource.name}</Typography>
                </FullWidthButton>
                }
            </ButtonBox>
            
        </StyledBox>
    )
}

export default TestSummaryPage
