import React from 'react'
import { Box, Typography } from '@material-ui/core'
import styled from 'styles/styled-components'
import FullWidthButton from 'components/Button/FullWidthButton'
import Background from '../assets/Quiz_Begin.svg';
import { ArrowForward } from '@material-ui/icons';
import messages from './Test.messages';
import { FormattedMessage } from 'react-intl';

const ImageDiv = styled(Box)`
    background-size: 100% 100%;
    background-image: url(${Background});
`

function TestStartPage(props:any) {
    const instructions = [messages.instruction1, messages.instruction2, messages.instruction3];
    let buttonText = null;
    if(props.fetchedState && props.fetchedState.status==='InProgress'){
        buttonText = messages.resumeButton;
    }else{
        buttonText = messages.startButton;
    }

    return (
        <Box  height='inherit' display='flex' flexDirection='column'>
        <Box flexGrow={1}>
            <ImageDiv position='relative' height='45%' minHeight='45%'>
            </ImageDiv>
            <Box overflow='auto' maxHeight='30%' height='30%' m={2}>
                {instructions && instructions.map((instruction,index)=>(
                    <Box display='flex' key={index} py={1}>
                        <Box pr={1}><ArrowForward/></Box>
                        <Box><Typography><FormattedMessage {...instruction}/></Typography></Box>
                    </Box>
                ))}
            </Box>
            <Box px={4}>
                <FullWidthButton
                    variant="contained"
                    color="secondary"
                    typovariant="h5"
                    padding={2}
                    onClick={()=>props.startTest()}>
                    <FormattedMessage {...buttonText}/>
                </FullWidthButton>
            </Box>
        </Box>
        </Box>
    )
}

export default TestStartPage
